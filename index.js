/*
* @Author: voery_liu
*/
module.exports = (api, option) => {
  // 通过链式修改webpack配置
  api.chainWebpack(webpackConfig => {
    webpackConfig.plugins.delete('pwa')
    webpackConfig.plugins.delete('workbox')
    webpackConfig.module
    .rule('images')
    .use('url-loader')
    .tap(options => Object.assign(options, { limit: 10240 }))
  })
  const { build } = api.service.commands
  // const serveFn = serve.fn
  const buildFn = build.fn
  api.registerCommand('pkg', {
    description: '将多页打包成多个文件',
    usage: 'vue-cli-service pkg [options]',
    options: {}
  }, args => {
    const path = require('path')
    const glob = require('glob')
    const cp = require('child_process')
    const fs = require('fs')
    // build.fn(...args)
    const BUILDLIST = []
    const getPages = () => {
      const PAGE_PATH = api.resolve('./src/pages')
      const entryFiles = glob.sync(PAGE_PATH + '/*/main.js')
      return entryFiles.map(entry => {
        const temp = entry.split('/')
        return {
          SPA: temp[temp.length - 2]
        }
      })
    }
    fs.access(api.resolve('./pkg.config.js'), err => {
      if (err) {
        console.log('请添加配置文件pkg.config.js来配置打包的文件')
      } else {
         BUILDLIST.push(...require('./pkg.config.js'))
      }
      if (!BUILDLIST.length) {
        BUILDLIST.push(...getPages())
      }
      let count = 0
     
      // console.log(BUILDLIST)
      BUILDLIST.forEach(item => {
        // console.log(process)
        const { SPA, publicPath = '/' + SPA, dest = 'dist/' + SPA } = item
        if (!SPA) return console.error('SPA 属性是必须的，请添加！')
        process.env.DEVPAGES = `["${SPA}"]`
        process.env.VUE_APP_PUBLICPATH = publicPath
        process.env.ASSETSROOT = dest
        process.env.isPkgBuild = true
        // 这里需要调用打包的方法，执行打包操作
        cp.exec('vue-cli-service build', (error, stdout) => {
          // console.log(error)
          count++
          if (count === BUILDLIST.length) {
            console.log('build done !!!')
          }
        })
      })
    })
  })
  build.fn = (...args) => {
    console.log('build fn')
    return buildFn(...args).then(res => {
    })
  }
}
module.exports.defaultModes = {
  pkg: 'production'
}