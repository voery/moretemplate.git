# 这是一个构建多页项目的vue-cli插件
## 插件简介
  1. 对webpack做了简单的配置
  2. 增加了pkg打包方法，可以将多页面单独打包。
    可以添加pkg.config.js文件来配置打包
      ```js
        // pkg.config.js
        module.exports = [
          {
            SPA: 'index',
            publicPath: '',
            dest: ''
          }
        ]
      ```

## 插件使用
  ```js
   vue add moretemplate
   vue invoke vue-cli-plugin-moretemplate
  ```