const path = require('path');
const glob = require('glob');

const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const isProd = process.env.NODE_ENV === 'production';
const {
  VUE_APP_PUBLICPATH = '/',
  DEVPAGES = [],
  AUTOPX2REM = true,
  PRODUCTIONGZIP = true,
  ASSETSROOT = 'dist',
  publicPath = '/',
  isPkgBuild = false
} = process.env;

const cssPlugins = [];
eval(AUTOPX2REM)
  && cssPlugins.push(
    require('postcss-px2rem-exclude')({
      remUnit: 100,
      remPrecision: 5,
      exclude: /node_modules/i,
    }),
  );

module.exports = {
  publicPath, // 默认是'/'
  outputDir: isPkgBuild ? ASSETSROOT : `${ASSETSROOT}${VUE_APP_PUBLICPATH}`, // 打包构建文件目录
  assetsDir: 'static', // 静态资源目录
  filenameHashing: false,
  pages: getPages(), // 多页面配置
  lintOnSave: !isProd,
  // transpileDependencies: [], // 显示转换依赖
  productionSourceMap: false, // 是否需要生产环境的source map
  configureWebpack: (config) => {
    // 配置webpack
    const plugins = [];
    isProd
      && plugins.push(
        // 内联脚本
        new HtmlWebpackInlineSourcePlugin(),
      );
    process.env.npm_config_report && plugins.push(new BundleAnalyzerPlugin());
    eval(PRODUCTIONGZIP)
      && plugins.push(
        new CompressionWebpackPlugin({
          filename: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.js$|\.json$|\.css$|\.ts$/,
          threshold: 10240,
          minRatio: 0.8,
          deleteOriginalAssets: false,
        }),
      );
    const dropConsolePlugin = [];
    isProd
      && dropConsolePlugin.push(
        new UglifyJsPlugin({
          uglifyOptions: {
            compress: {
              drop_debugger: true,
              drop_console: true,
              pure_funcs: ['console.log'],
            },
          },
          sourceMap: true,
          parallel: true,
        }),
      );
    config.resolve.extensions = ['.js', '.vue', '.json', '.ts', '.tsx'];
    config.module.rules.push({
      test: /\.md$/,
      loader: 'text-loader'
    });
    return {
      resolve: {
        alias: {
          'public@': path.join(__dirname, './public'),
          'page@': path.join(__dirname, './src/pages'),
          'index@': path.join(__dirname, './src/pages/index')
        },
      },
      module: {},
      optimization: {
        minimize: true,
        minimizer: dropConsolePlugin,
        splitChunks: {
          minSize: 30000,
          cacheGroups: {
            vendors: {
              name: 'vendors',
            },
          },
        },
        runtimeChunk: {
          // 这里的名字要与下面的一样
          name: 'runtime',
        },
      },
      plugins,
    };
  },
  chainWebpack: (config) => {
    config.resolve.symlinks(true);
    const pages = Object.keys(getPages());
    pages.forEach((page) => {
      config.plugin(`preload-${page}`).tap((args) => {
        // 这里的配置可能导致main.ts无法运行
        args[0].fileBlacklist.push(/runtime.*\.js$/);
        return args;
      });
    });
    // copy的逻辑
    config.plugin('copy').tap(args => {
      const { from, to, toType } = args[0][0]
      args[0][0].from = from + '/static/'
      args[0][0].to = to + '/static/'
      if (isPkgBuild) {
        args[0].push({
          from: `${from}/${pages[0]}`,
          to: `${to}/${pages[0]}`,
          toType
        })
      }
      return args
    })
  },
  css: {
    extract: isProd,
    sourceMap: false,
    loaderOptions: {
      postcss: {
        plugins: cssPlugins,
      },
    },
  },
  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'stylus',
      // patterns: [path.resolve(__dirname, './src/assets/css/global.less')]
    },
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://0.0.0.0:8080',
        ws: true,
        changeOrigin: true,
      },
    },
  },
};
function getPages() {
  const PAGE_PATH = path.resolve(__dirname, './src/pages');
  const entryFiles = glob.sync(`${PAGE_PATH}/*/main.js`);
  const devPages = eval(DEVPAGES);
  return entryFiles.reduce((pages, entry) => {
    const temp = entry.split('/');
    const filename = temp[temp.length - 2];
    if (!devPages.length || devPages.indexOf(filename) > -1) {
      const chunks = ['runtime', 'flexible', 'vendors', filename];
      pages[filename] = {
        entry,
        template: entry.replace(/main\.js$/, 'index.html'),
        chunks,
        chunksSortMode: (a, b) => chunks.indexOf(a.names[0]) - chunks.indexOf(b.names[0]),
        inlineSource: isProd ? '(runtime|flexible).*.js$' : '',
      };
    }
    return pages;
  }, {});
}
