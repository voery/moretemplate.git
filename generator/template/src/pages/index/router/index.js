import Vue from 'vue'
import VueRouter from 'vue-router'
import Nprogress from 'nprogress'
import Layout from '../components/Layout.vue'
Nprogress.inc(0.2)
Nprogress.configure({ easing: 'ease', speed: 500, showSpinner: true })

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '',
    component: Layout,
    children: genRouters({ include: [], exclude: [] })
  }
]
const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})
// 跳转路由前
router.beforeEach((to, from, next) => {
  Nprogress.start()
  next()
})
// 跳转路由后
router.afterEach(route => {
  Nprogress.done()
})
function genRouters({ include, exclude }) {
  const contexts = require.context(
    '../views',
    true,
    /\.\/(((?!\/).)*|(.*)?children\/(.*)?)\/index\.vue$/,
    'lazy'
  )
  const routers = []
  const pathArray = contexts
    .keys()
    .map(dir => {
      const arr = dir
        .slice(dir.indexOf('/') + 1, dir.lastIndexOf('/'))
        .split('/')
      return filterRouters(arr[0]) ? arr : []
    })
    .sort((a, b) => a.length - b.length)
  function filterRouters(router) {
    if (!include.length) return exclude.indexOf(router) === -1
    if (include.length && !exclude.length) return include.indexOf(router) > -1
    if (include.length && exclude.length)
      return exclude.indexOf(router) === -1 && include.indexOf(router) > -1
  }
  function path2Routers(arr, result = [], i = 0) {
    let el = arr[i++]
    if (!el) return
    if (el === 'children') {
      el = arr[i++]
    }
    const filterItem = result.filter(item => item.name === el)[0]
    if (filterItem) {
      path2Routers(arr, filterItem.children, i)
    } else {
      const path = arr.slice(0, i).join('/')
      const obj = {
        name: el,
        children: [],
        path: '/' + path.replace(/\/children/g, ''),
        component: () =>
          import(
            // 下面的写法对babel-eslint有限制
            /* webpackChunkName: "index-[request]" */
            /* webpackInclude: /children(\/|\\).*(\/|\\)index\.vue$/ */
            `../views/${path}`
          )
      }
      result.push(obj)
      path2Routers(arr, obj.children, i)
    }
    return result
  }
  pathArray.forEach(item => {
    if (!item.length) return
    const name = item[0]
    if (item.length === 1) {
      routers.push({
        name,
        children: [],
        path: `/${name}`,
        component: () =>
          import(
            /* webpackChunkName: "index-[request]" */
            /* webpackInclude: /views(\/|\\)((?!(\/|\\)).)*(\/|\\)index\.vue$/ */
            `../views/${name}`
          )
      })
    } else {
      path2Routers(item, routers)
    }
  })

  return routers
}

export default router
