/*
* @Author: voery_liu
*/
module.exports = (api, options, rootOptions) => {
  api.render('./template')
  // 修改`package.json`中的字段
  api.extendPackage({
    scripts: {
      dev: 'vue-cli-service serve',
      pkg: 'vue-cli-service pkg'
    },
    dependencies: {
      "vue-router": "^3.3.2",
      "vuex": "^3.4.0"
    },
    devDependencies: {
      "less-loader": "^6.1.0",
      "nprogress": "^0.2.0",
      "compression-webpack-plugin": "^3.1.0",
      "html-webpack-inline-source-plugin": "^0.0.10",
      "postcss-px2rem-exclude": "^0.0.6",
      "uglifyjs-webpack-plugin": "^2.2.0",
      "webpack-bundle-analyzer": "^3.6.0"
    }
  })
  api.onCreateComplete(() => {
    const fs = require('fs')
    const path = require('path')
    const copyFile = (src, dist) => {
      // console.log(path.resolve(src), dist, 'copyFile')
      fs.writeFileSync(dist, fs.readFileSync(api.resolve(src)))
    }
    const delFile = (src, callback) => {
      try {
        fs.accessSync(src)
        fs.unlinkSync(src)
      } catch (error) {
        callback(error)
      }
    }
    delFile(api.resolve('./src/main.js'), console.log)
    delFile(api.resolve('./src/App.vue'), console.log)
    // 项目创建成功后的回调函数，可以在这里添加一些必要的东西
    const vueConfigPath = api.resolve('./vue.config.js')
    if (!fs.existsSync(vueConfigPath)) {
      copyFile('./node_modules/vue-cli-plugin-moretemplate/vue.config.js', vueConfigPath)
    }
  })
}